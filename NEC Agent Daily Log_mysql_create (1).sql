CREATE TABLE `employees` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`position` varchar(191) NOT NULL,
	`department` varchar(191) NOT NULL,
	`location` varchar(191) NOT NULL,
	`reports_to` int(11) NOT NULL,
	`joining_date` DATE NOT NULL,
	`employment_status` varchar(191) NOT NULL,
	`person_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(191) NOT NULL,
	`password` varchar(191) NOT NULL,
	`shift_schedule` varchar(191) NOT NULL,
	`role_id` INT(11) NOT NULL,
	`employee_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `roles` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`role_name` varchar(191) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `attendance_data` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`date` DATE NOT NULL,
	`log_in` TIMESTAMP NOT NULL,
	`log_out` TIMESTAMP NOT NULL,
	`status` varchar(191) NOT NULL,
	`tardiness` TIME,
	`user_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `shift_schedules` (
	`shift` varchar(191) NOT NULL,
	`start_time` TIME NOT NULL,
	`end_time` TIME NOT NULL,
	PRIMARY KEY (`shift`)
);

CREATE TABLE `leave_statuses` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`type_of_leave` varchar(191) NOT NULL,
	`start_date` DATE NOT NULL,
	`end_date` DATE NOT NULL,
	`user_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `personal_info` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(191) NOT NULL,
	`last_name` varchar(191) NOT NULL,
	`birthday` DATE NOT NULL,
	`email` varchar(191) NOT NULL,
	`status` varchar(191) NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `employees` ADD CONSTRAINT `employees_fk0` FOREIGN KEY (`reports_to`) REFERENCES `employees`(`id`);

ALTER TABLE `employees` ADD CONSTRAINT `employees_fk1` FOREIGN KEY (`person_id`) REFERENCES `personal_info`(`id`);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`shift_schedule`) REFERENCES `shift_schedules`(`shift`);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk1` FOREIGN KEY (`role_id`) REFERENCES `roles`(`id`);

ALTER TABLE `Users` ADD CONSTRAINT `Users_fk2` FOREIGN KEY (`employee_id`) REFERENCES `employees`(`id`);

ALTER TABLE `attendance_data` ADD CONSTRAINT `attendance_data_fk0` FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`);

ALTER TABLE `leave_statuses` ADD CONSTRAINT `leave_statuses_fk0` FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`);

