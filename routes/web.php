<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|    return view('welcome');
*/

Route::get('/', 'BasicUserDashboardController@home');

Route::get('/home', 'BasicUserDashboardController@home');
				// ->middleware('auth','basicuser');
    			


Route::get('/dashboard', 'DashboardController@index');


Route::get('/admin-dashboard', 'DashboardController@index')
			->middleware('auth','admin');

Route::get('/clockin', 'ClockInController@login');
Route::get('/clockout', 'ClockInController@logout');


Route::get('/team_attendance_report/sort', 'teamAttendanceController@sort');
Route::get('/myprofile', 'Personal_infoController@index');
Route::get('/teamdata', 'teamAttendanceController@index')
			->middleware('auth','admin');


    			
Route::get('/ajxfilter', 'teamAttendanceController@ajaxfilter');



Route::get('/tlhomepage', 'TeamLeaderHomepageController@index')
				->middleware('auth','admin');

Route::get('/leaveRequests', 'LeaveRequestsList@index')
				->middleware('auth','admin');
				
Route::post('/approveleave', 'LeaveRequestsList@approve');
Route::post('/denyleave', 'LeaveRequestsList@deny');


Route::get('/registerbyadmin', 'RegisterbyAdminController@supervisorlist')->name('custom.register');
Route::post('/registerbyadmin', 'RegisterbyAdminController@register');


Route::get('/applyleave', 'LeaveApplicationsController@index')
			->middleware('auth');

Route::post('/applyleave', 'LeaveApplicationsController@store')->name('applyleave');

Route::get('/applyleave-delete', 'LeaveApplicationsController@deleterequest');

Route::get('/applyleave-edit', 'LeaveApplicationsController@editrequest');
Route::post('/applyleave-edit', 'LeaveApplicationsController@editrequest');


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/teammemberprofile', 'TeamLeaderHomepageController@tmemberProfile')
			->middleware('auth','admin');

Route::post('/teammemberprofile', 'TeamLeaderHomepageController@tmemberProfile');

Route::post('/teammemberprofile-delete', 'TeamLeaderHomepageController@tmemberProfileDelete');
Route::post('/teammemberprofile-edit', 'TeamLeaderHomepageController@tmemberProfileEdit');



Route::get('/calendar', 'CalendarController@index')
			->middleware('auth','admin');

Route::get('/load', 'LeaveRequestsList@load');

Route::post('/updateprofile', 'Personal_infoController@changeimage');
