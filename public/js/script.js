  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
      edge: 'left'
    });
  });


    document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {
      dismissible:true
      });
  });


  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var instances = M.Collapsible.init(elems, {
  accordion: false
  });
  });

$.ajaxSetup({
	headers:{
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});


function approve(id){

	const requestid = id;
	const approvers_message = $('#approvers_message-'+requestid).val();

	$.ajax({
		url: '/approveleave',
		method: 'POST',
		data: {"leaveId": requestid, "message": approvers_message }
	}).done( function(data){


    $('#modal-notif').html('Leave is now plotted in the calendar');
    $('#modal-notif').css('text-align','center');


    $("#leaveitemno-"+requestid).remove();
    var classcount = $('.leaveitems').length;
    
    
    if (classcount === 0) {
      $('#noleaverequestnote').html('No leave request in queue.');
      $('#noleaverequestnote').css('color','white');


    }

	});

	
}


function deny(id){

	const requestid = id;
	const approvers_message = $('#approvers_message-'+requestid).val();

	$.ajax({
		url: '/denyleave',
		method: 'POST',
		data: {"leaveId": requestid, "message": approvers_message }
	}).done( function(data){


    $('#modal-notif').html('Leave request has been denied by you.');
    $('#modal-notif').css('text-align','center');


    $("#leaveitemno-"+requestid).remove();
    var classcount = $('.leaveitems').length;
    
    
    if (classcount === 0) {
      $('#noleaverequestnote').html('No leave request in queue.');
      $('#noleaverequestnote').css('color','white');


    }

	});

	
}


$('#add-task').click(function(){
	const name = $('#newTask').val();

	$.ajax({
		url: '/tasks',
		method: 'POST',
		data: {name: name}
	}).done( function(data){

		$('#tbody').html(data);

	});
});






  

function startTime() {
    var today = new Date();
    var h = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
    var am_pm = today.getHours() >= 12 ? "PM" : "AM";
    h = h < 10 ? "0" + h : h;
    var m = today.getMinutes() < 10 ? "" + today.getMinutes() : today.getMinutes();
    var s = today.getSeconds() < 10 ? "" + today.getSeconds() : today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h + ":" + m + ":" + s + " " + am_pm;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}


function confirmssges(id){

  const requestid = id
  $('#modfooterdelrequest').show();
  $('#confirmnote').html('Are you sure you want to Delete this data?' + requestid);
  $('#testid').val(requestid);


}
function delete_request(){

  const requestid = $('#testid').val();
  const approvers_message = $('#approvers_message-'+requestid).val();

  $('#deleteconfirmmodal').modal('close');
  $('#confirmnote').html('Leave request now deleted.' );

  $('#modfooterdelrequest').hide();
  $('#deleteconfirmmodal').modal('open');

        $.ajax({
          url: '/applyleave-delete',
          method: 'GET',
          data: {id: requestid},
          success:function(data){
          

            // window.location.reload()
              $("#leaverequest-"+requestid).remove();
              var classcount = $('.leaverequest').length;
              
              
              if (classcount === 0) {
                $('#norequest').html('No leave request.');
                $('#norequest').css('color','white');


              }

        }
        });
    
}





function editrequest(id){

  const requestid = id;
  const approvers_message = $('#approvers_message-'+requestid).val();

  const typeofleave = $('#typeofleave-'+requestid).html()
  const reasonedit = $('#reason-edit-'+requestid).html()
  const numofdaysedit = $('#numofdays-edit-'+requestid).html()
  const msgtoapproveredit = $('#msgtoapprover-edit-'+ requestid).html()
  const requesteditid = $('#requestid-edit-'+ requestid).val()


  // alert(typeofleave);
  $('#typeofleave_edit').val(typeofleave);
  $('#mssgetoapprover').val(msgtoapproveredit);
  $('#reasonedit').val(reasonedit)
  $('#numbeofdaysedit').val(numofdaysedit);
  $('#requestidedit').val(requesteditid);
  

}


function saveeditrequest(){

  const type_of_leave = $('#typeofleave_edit').val();
  const startdate = $('#startdateedit').val();
  const enddate = $('#enddateedit').val();
  const numofdays = $('#numbeofdaysedit').val();
  const reason = $('#reasonedit').val()
  const requestor_message = $('#mssgetoapprover').val();
  const requestid =   $('#requestidedit').val();

  
        $.ajax({
          url: '/applyleave-edit',
          method: 'POST',
          data: {'requestid':requestid,'type_of_leave': type_of_leave,'startdate':startdate, 'enddate':enddate, 
                 'numofdays':numofdays, 'reason':reason, 'requestor_message':requestor_message }
          }).done( function(data){
              

             $('#typeofleave-'+requestid).html(type_of_leave)
             $('#reason-edit-'+requestid).html(reason)
             $('#numofdays-edit-'+requestid).html(numofdays)
             $('#msgtoapprover-edit-'+ requestid).html(requestor_message)
             $('#startdateedit-'+ requestid).html(startdate)
             $('#enddateedit-'+ requestid).html(enddate)

             $('#editrequestmodal').modal('close');
             $('#deleteconfirmmodal').modal('open');
             $('#confirmnote').html('Leave request is now updated.');
             $('#modfooterdelrequest').hide();
        
        });


}

function cancel(){

  $('#deleteconfirmmodal').modal('close');
}
