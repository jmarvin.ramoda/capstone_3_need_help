@extends('layouts.main_layout')

@section('content')

		<div class="row">
	      <div class="col s12 m12 l12">
	      	 <div class="row">
	      	<div class="col s12 m12 l3">
		      <div class="card">
		        <div class="card-content" style="height: 100%">



@php
	if ($user_attendance == null ) {
		$user_log_in = null;
    	$user_log_datetoday = null;
	}else{

	    $user_log_in = $user_attendance->log_in;
	    $user_log_datetoday = $user_attendance->date;

	    $phplogin = strtotime($user_log_in);
	    $convert_login = date('h:i:s A', $phplogin);

    }
@endphp

  
    @if ($user_log_datetoday == null || $user_log_in == null) 
        
	    <h5>You're not logged in yet.</h5>
    	<a href="/home" class="btn">Log In Now</a>

    @else

	    <h5>You logged in today at</h5>
    	<h4> {{$convert_login}} </h4>
 
    

    @endif


		        </div>

		      </div>
		    </div>
	     
	      	<div class="col s12 m12 l4">
		      <div class="card">
		        <div class="card-content white-text remaining-leave">

		        	<div class="">
		        		<canvas id="ApproveLeaveChart"></canvas>
		        	</div>

		        </div>

		      </div>
		    </div>

	      	<div class="col s12 m12 l4">
		      <div class="card">
		        <div class="card-content white-text remaining-leave">

		        	<div class="">
		        		<canvas id="SickLeaveChart"></canvas>
		        	</div>

		        </div>

		      </div>
		    </div>

	    
		</div>

		  </div>
		
		<div class="col s12 m12 l12">  
	      <div class="row">

	
	      	<div class="col s12 m12 l7">
		      <div class="card">
		      	<div class="card-content">
			      	<h5>Attendance Performance</h5>
		        	<div class="">
		        		<canvas id="attendancechart"></canvas>
		        	</div>
			    </div>
		      </div>
		    </div>


	      	<div class="col s12 m12 l4">
		      <div class="card">
		      	<div class="card-content">

		      	<h5>Upcoming Leave</h5>
			      <div class="collection">

@php
	use Carbon\Carbon;

    $dt = Carbon::now('UTC');
    $realtimeDate = $dt->addHours(8);

    $phpdatetoday = strtotime($realtimeDate);
    $convert_dateToday = date('l, F, j Y', $phpdatetoday);
    $datetoday = $convert_dateToday;


@endphp    

	 

    @foreach ($leaverecords as $leaverecord) 

        @php
        $phpdate = strtotime($leaverecord->start_date);
        $convert_start_date = date('F, j Y', $phpdate);

        $leavestartdates = $convert_start_date;

        $phpdate = strtotime($leaverecord->end_date);
        $convert_enddate = date('F, j Y', $phpdate);

        $leaveenddates = $convert_enddate;@endphp 


    @if ($phpdate > $phpdatetoday) 

        	<table style="table-layout: fixed;">
        		<tr >
        			<td ><i class="fas fa-plane-departure" style="color: green;"></i> {{$leaverecord->type_of_leave}}</td>
        			<td >{{$leavestartdates}}</td>
        			<td style="text-align: center; width: 10%">to</td>
        			<td >{{$leaveenddates}}</td>
        		</tr>
        	</table>
    @else


 	@endif

@endforeach
			      
      
    

   	
			      </div>
			      </div>


		      </div>
		    </div>

		  </div>

		</div>


		</div>

<script type="text/javascript">

	var SickLeaveChart = document.getElementById('SickLeaveChart').getContext('2d');
	var leaveCount = 2;
	var leaveAvailable = 15
	// Chart.defaults.global.defaultFontFamily ='Lato';
	// Chart.defaults.global.defaultFontSize = 10;
	Chart.defaults.global.defaultFontColor = '#777';

	var massPopChart = new Chart(SickLeaveChart,{
		type: 'doughnut', //bar, horizontalbar, pie, line, doughnut
		data:{
			labels:['Used Planned Leave','Leave Available'],
			datasets:[{
				label:'Planned Leave Usage',
				data:[
					leaveCount,
					leaveAvailable,

					],
					// backgroundColor:'green'
					backgroundColor:[
						'rgba(254, 39, 35, 0.6)',
						'rgba(74, 62, 345, 0.6)',
					],

					borderWidth:1,
					borderColor:'#777',
					hoverBorderWidth: 3,
					hoverBorderColor:'#000'

			}],

		},
		options:{
			title:{
				display:true,
				text:'Sick Leave Credits',
				fontSize:15
			},

			legend:{
				display:true,
				position:'right',
				labels:{
					fontColor:'#000',
					fontSize:10
				}
			},
			layout:{
				padding:{
					left:0,
					right:0,
					bottom:0,
					top:0
				}
			},
			tooltips:{
				enabled:true
			}
		}

	});



	var ApproveLeaveChart = document.getElementById('ApproveLeaveChart').getContext('2d');
	var leaveCount = 2
	var leaveAvailable = 15 
	// Chart.defaults.global.defaultFontFamily ='Lato';
	// Chart.defaults.global.defaultFontSize = 10;
	Chart.defaults.global.defaultFontColor = '#777';




	var massPopChart = new Chart(ApproveLeaveChart,{
		type: 'pie', //bar, horizontalbar, pie, line, doughnut
		data:{
			labels:['Used Planned Leave','Leave Available'],
			datasets:[{
				label:'Planned Leave Usage',
				data:[
					leaveCount,
					leaveAvailable,

					],
					// backgroundColor:'green'
					backgroundColor:[
						'rgba(255, 99, 132, 0.6)',
						'rgba(54, 162, 235, 0.6)',
					],

					borderWidth:1,
					borderColor:'#777',
					hoverBorderWidth: 3,
					hoverBorderColor:'#000'

			}],

		},
		options:{
			title:{
				display:true,
				text:'Planned Leave Credits',
				fontSize:15
			},

			legend:{
				display:true,
				position:'right',
				labels:{
					fontColor:'#000',
					fontSize:10
				}
			},
			layout:{
				padding:{
					left:0,
					right:0,
					bottom:0,
					top:0
				}
			},
			tooltips:{
				enabled:true
			}
		}

	});



	var attendancechart = document.getElementById('attendancechart').getContext('2d');
	var numofworkingdays = {{$numberofworkingdays}};
	// var leaveCount = 2
	// var leaveAvailable = 15 
	// Chart.defaults.global.defaultFontFamily ='Lato';
	// Chart.defaults.global.defaultFontSize = 10;
	Chart.defaults.global.defaultFontColor = '#777';
	Chart.defaults.global.showLines = true;


	var massPopChart = new Chart(attendancechart,{
		type: 'horizontalBar', //bar, horizontalBar, pie, line, doughnut
		data:{
			labels:['Number of Working Days','Present', 'Absent'],
			datasets:[{
				label:'Planned Leave Usage',
				data:[
					numofworkingdays,
					12,
					9
					],		
					// backgroundColor:'green'
					backgroundColor:[
						'lightblue',
						'green',
						'red',
					],

					borderWidth:1,
					borderColor:'#777',
					hoverBorderWidth: 3,
					hoverBorderColor:'#000'

			}],

		},
		options:{
			title:{
				display:true,
				text:'Actual Number of Days Present vs. Days Absent',
				fontSize:15
			},

			legend:{
				display:true,
				position:'right',
				labels:{
					fontColor:'#000',
					fontSize:10
				}
			},
			layout:{
				padding:{
					left:0,
					right:0,
					bottom:0,
					top:0
				}
			},
			tooltips:{
				enabled:true
			}
		}

	});

</script>


@endsection