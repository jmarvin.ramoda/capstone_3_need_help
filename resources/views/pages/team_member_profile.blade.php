@extends('layouts.main_layout-nosidebar')

@section('content')

    <div class="container">
	    <div class="row">

	      <div class="col s12 m12 l3" id="emp-profile-card">
		      <div class="card" style="background-color: #FEFFFE">
		        <div class="card-content thumbnail">

					<div id="profile-photo">
						<img src="images/{{$users->avatar}}">
						<p id="tmemberprofilename">{{$users->username}}</p>
						{{-- <small>{{$employees->position}}</small> --}}
					</div>

		        </div>

		      </div>

		      <div id="sidelist-tmprofile">
		      	  <h5>Pages</h5><hr>
			      <ul >
				      	<li><a href="">Dashboard</a></li>
				      	<li><a href="/tlhomepage">My Team</a></li>
				      	<li><a href="/leaveRequests">Leave Requests</a></li>
				      	<li><a href="/calendar">Leave Calendar</a></li>
	
			      </ul>
				  

				  <h5>Action</h5><hr>
				  <ul>
				      	<li><a class="waves-effect waves-light modal-trigger" href="#confirmationmodal">Delete Member</a></li>
				      	<li><a onclick="editData()" class="waves-effect waves-light modal-trigger" href="#editprofilemodal">Edit Profile</a></li>

			      </ul>
			  </div>

		  </div>

	   


	      <div class="col s12 m12 l8">
		      

		        <div class="card" style="">
		        	<div class="card-content">

				      <div class="row">

				        <div class="col s12 m6 l12">
				        	<h5 class="employee-data-header">Personal Details</h5>
					        <div class="row">
						        <div class="col s5 m6 l6">
								@php
					        		$phpdate_birthday = strtotime($person->birthday);
									$birthday = date('F j, Y', $phpdate_birthday);
					        	@endphp		        	



					        		<ul>
					        			<li class="employee-data-label">First Name</li>
					        			<li class="employee-data-label">Last Name</li>
					        			<li class="employee-data-label">Date of Birth</li>
					        			<li class="employee-data-label">Marital Status</li>
					        			<li class="employee-data-label">Email Address</li>
					        		</ul>
					        	</div>



					        	<div class="col s7 m6 l6">
					        		<ul>
					        			<p id="fname" class="employee-data-profile"> {{$person->first_name}} </p>
					        			<p id="lname" class="employee-data-profile"> {{$person->last_name}} </p>
					        			<p id="bday" class="employee-data-profile">{{$birthday}}</p>
					        			<p id="stats" class="employee-data-profile"> {{$person->status}} </p>
					        			<p id="email" class="employee-data-profile">{{$person->email}}</p>
					        		</ul>

					        	</div>
				        	</div>
				        </div>
				        	
				        <div class="col s12 m6 l12">
				        	<h5 class="employee-data-header">Employment Details</h5>
					        <div class="row">			        
					        	<div class="col s5 m6 l6">
					        		
					        		<ul>
					        			<li></li>
					        			<li class="employee-data-label">Employee ID</li>
					        			<li class="employee-data-label">Position</li>
					        			<li class="employee-data-label">Department</li>
					        			<li class="employee-data-label">Location</li>
					        			<li class="employee-data-label">Reporting To</li>
					        			<li class="employee-data-label">Joining Date</li>
					        			<li class="employee-data-label">Employment Status</li>
					        			
					        		</ul>

					        	</div>

					        	@php
					        		$phpdate_joiningdate = strtotime($employees->joining_date);
									$joining_date = date('F j, Y', $phpdate_joiningdate);
					        	@endphp

					        	<div class="col s7 m6 l6">

					        		
					        			<p id="eid" class="employee-data-profile"> {{$employees->id}} </p>
					        			<p id="position" class="employee-data-profile"> {{$employees->position}}</p>
					        			<p id="dept" class="employee-data-profile">{{$employees->department}}</p>
					        			<p id="location" class="employee-data-profile">{{$employees->location}}</p>
					        			<p id="sup-name" class="employee-data-profile"> {{$sup->first_name}} {{$sup->last_name}}</p>
					        			<p id="joindate" class="employee-data-profile">{{$joining_date}}</p>
					        			<p id="emp-status" class="employee-data-profile">{{$employees->employment_status}}</p>
					  
				        	</div>


	
		      				</div>
		        </div>
		      </div>


	    </div>
    </div>
    </div>
    </div>
 </div>



  <!-- Modal Structure -->
  <div id="confirmationmodal" class="modal">
    <div class="modal-content">

    <h5>Are you sure you want to delete this member from your team?</h5>
	<form action="/teammemberprofile-delete" method="POST" >
		{{ csrf_field() }}
		<input type="hidden" name="memberuserid" value="{{$users->id}}" />
		<button type="submit" class="btn" style="margin-top: 20px;">Proceed</button>
		<a onclick="closeMod()" href="#" class="btn" style="margin-top: 20px;">Cancel</a>
	</form>

    </div>

  </div>


  <!-- Modal Structure -->
  <div id="editprofilemodal" class="modal" >
    <div class="modal-content" >

    <h5>Edit Profile</h5>
    
	<form action="/teammemberprofile-edit" method="POST" >
		{{ csrf_field() }}
	
	<button class="btn" name="action" type="submit" style="float: right;">Save</button>
	<input type="hidden" name="memberuserid" value="{{$users->id}}" />
			  <div class="row">
		      <div class="col s12 m4 l12">
		      
			      <div class="">
			        
				        <div class="">

				        	<div class="row">

				        		<div class="col s12 m4 l12">
				        			<h5>Personal Information</h5>

						        	<div class="row">
						        		
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>First Name</label>
						        			<input id="fname-edit" class="registerbyadmininput" type="text" name="fname" value="{{old('fname')}}">
						        		</div>
						       
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Last Name</label>
				        					<input id="lname-edit" class="registerbyadmininput" type="text" name="lname" value="{{old('lname')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Birthday</label>
				        					<input id="bday-edit" class="registerbyadmininput" type="date" name="dob" value="{{old('dob')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Marital Status</label>
				        					<input id="stats-edit" class="registerbyadmininput" type="text" name="mstatus" value="{{old('mstatus')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Email Address</label>
				        					<input id="email-edit" class="registerbyadmininput" type="text" name="email" value="{{old('email')}}">

						        		</div>

						        	</div>

				        			<h5>Employment Information</h5>
						        	<div class="row">
						        		
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Designation</label>
						        			<input id="position-edit" class="registerbyadmininput" type="text" name="position" value="{{old('position')}}">
						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Department</label>
				        					<input id="dept-edit" class="registerbyadmininput" type="text" name="department" value="{{old('department')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Location</label>
				        					<input id="location-edit" class="registerbyadmininput" type="text" name="location" value="{{old('location')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Position/Rank</label>
						        			<select style="display: block;" name="rank" value="{{old('rank')}}">

						        				<option disabled selected>Select Rank</option>
						        				<option>Team Member</option>
						        				<option>Supervisor</option>
						        				<option>Manager</option>
						        				<option>Director</option>
						        			</select>
						        		</div>

						       
						       @php
						       	use App\Personal_info;
						       @endphp
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Reports To</label>

				        					<select name="reports_to" style="display: block;" value="{{old('reports_to')}}">

				        						<option disabled selected>Select Supervisor Name</option>
			
				        						@foreach ($teamleaders as $teamleader) 
				        							@php
														$person =  $teamleader->person_id;
														$test = Personal_info::where('id', $person)->first();
													@endphp
													
													<option id="emp-{{$teamleader->id}}" value="{{$teamleader->id}}"> 
														{{$test->first_name}} {{$test->last_name}}
													</option>
				        						@endforeach
				        					</select>

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Joining Date</label>
				        					<input id="joindate-edit" class="registerbyadmininput" type="date" name="joindate" value="{{old('joindate')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Employment Status</label>
						        			<select class="registerbyadmininput" style="display: block; color: black" name="empstatus" value="{{old('empstatus')}}">

						        				<option disabled selected>Select Employment Status</option>
						        				<option>Permanent</option>
						        				<option>Probationary</option>
						        				<option>Project Based</option>
						        			</select>
		

						        		</div>


						        	</div>
				        			

						        	<div class="row">

						        		<div class="col s12 m4 l12" style="margin: 0 auto;">
											

						        		</div>

						        	</div>


				        		</div>
				        	</div>

				        </div>

			      </div>
	
		  	  </div>
		  	  </div>

	</form>

    </div>

  </div>

<script type="text/javascript">
	
	function closeMod(){

		$('#confirmationmodal').modal('close');
	}


	function editData(){


	const fname	= $('#fname').html()
	const lname = $('#lname').html()
	const bday = $('#bday').html()
	const stats = $('#stats').html()
	const email = $('#email').html()
	const eid = $('#eid').html()
	const position = $('#position').html()
	const dept = $('#dept').html()
	const location = $('#location').html()
	const supname = $('#sup-name').html()
	const joindate = $('#joindate').html()
	const empstatus = $('#emp-status').html()


	$('#fname-edit').val(fname);
	$('#lname-edit').val(lname);
	$('#bday-edit').val(bday);
	$('#stats-edit').val(stats);
	$('#email-edit').val(email);
	$('#position-edit').val(position);
	$('#dept-edit').val(dept);
	$('#location-edit').val(location);
	$('#joindate-edit').val(joindate);



	}


</script>
@endsection