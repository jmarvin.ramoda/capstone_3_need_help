@extends('layouts.main_layout')

@section('content')


	    <div class="row ">
	      <div class="col s12 m1 l8">
	      </div>
	      <div class="col s12 m10 l8">

		      <div class="card blue-grey darken-1">
		        <div class="card-content white-text">

		        	<div class="row">
				      <div class="col s12 m8 l9">
				      	<h5>Leave Request</h5>
				      </div>
				<form method="POST" action="{{route('applyleave')}}">
					{{ csrf_field() }}
				      <div class="col s12 m4 l3" >
				      	<button class="btn approval-btn" style="width: 100%; height: 100%; font-size: 12px; padding: 0px;" >Send For Approval</button>
				      </div>

			        	<div class="col s12 m6 l12 leave-form">
					        <label>Type of Leave</label>
					        <select name="type_of_leave" style="display: block;">
					        	<option selected disabled>Select leave type</option>
					        	<option value="1" {{ old('type_of_leave') == 1 ? 'selected' : '' }}>Vacation</option>
					        	<option value="2" {{ old('type_of_leave') == 2 ? 'selected' : '' }}>Maternity</option>
					        	<option value="3" {{ old('type_of_leave') == 3 ? 'selected' : '' }}>Paternity</option>
					        	<option value="4" {{ old('type_of_leave') == 4 ? 'selected' : '' }}>Solo Parent</option>
					        	<option value="5" {{ old('type_of_leave') == 5 ? 'selected' : '' }}>Bereavement</option>
					        	<option value="6" {{ old('type_of_leave') == 6 ? 'selected' : '' }}>Leave of Absence</option>
					        </select>

                        @if ($errors->has('type_of_leave'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type_of_leave') }}</strong>
                            </span>
                        @endif

					    </div>

					    <div class="col s12 m6 l6 leave-form">
					    		<label>Start Date</label>
					        	<input class="datepick" type="date" name="startdate" value="{{old('startdate')}}">
	                    
	                    @if ($errors->has('startdate'))
                            <span class="help-block">
                                <strong>{{ $errors->first('startdate') }}</strong>
                            </span>
                        @endif				   


					    </div>

					    <div class="col s12 m6 l6 leave-form">
					    		
					    		<label>End Date</label>
					        	<input class="datepick" type="date" name="enddate" value="{{old('enddate')}}">

	                    @if ($errors->has('enddate'))
                            <span class="help-block">
                                <strong>{{ $errors->first('enddate') }}</strong>
                            </span>
                        @endif	

					    </div>

					    <div class="col s12 m6 l6 leave-form">
					    		
					    		<label>Number of Days</label>
					        	<input class="datepick" type="number" name="numofdays" min="0" value="{{old('enddate')}}">

	                    @if ($errors->has('numofdays'))
                            <span class="help-block">
                                <strong>{{ $errors->first('numofdays') }}</strong>
                            </span>
                        @endif

					    </div>

					    <div class="col s12 m6 l6 leave-form">
					    		
					    		<label>Reason</label>

						        <select name="reason">
						        	<option selected disabled>Select reason</option>
						        	<option value="1" {{ old('reason') == 1 ? 'selected' : '' }}>Leisure</option>
						        	<option value="2" {{ old('reason') == 2 ? 'selected' : '' }}>Study</option>
						        	<option value="3" {{ old('reason') == 3 ? 'selected' : '' }}>Process Paperworks</option>
						        	<option value="4" {{ old('reason') == 4 ? 'selected' : '' }}>Others</option>

						        </select>

	                    @if ($errors->has('reason'))
                            <span class="help-block">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        @endif

					    </div>

					    <div class="col s12 m6 l12 leave-form">
					    		
					    		<label>Approver</label>

		        					<select name="reports_to" style="display: block;" value="{{old('reports_to')}}">

		        						<option disabled selected>Select Supervisor Name</option>
	
		        						@php
        								 use App\Personal_info;

        								@endphp 

		        						@foreach ($teamleaders as $teamleader) 
		        							@php
												$person =  $teamleader->person_id;
												$test = Personal_info::where('id', $person)->first();
											@endphp
											
							<option id="emp-{{$teamleader->id}}" value="{{$teamleader->id}}" {{ old('reports_to') == $teamleader->id ? 'selected' : '' }}> 
												{{$test->first_name}} {{$test->last_name}}
							</option>
		        						@endforeach
		        					</select>

	                    @if ($errors->has('reports_to'))
                            <span class="help-block">
                                <strong>{{ $errors->first('reports_to') }}</strong>
                            </span>
                        @endif

					    </div>

					    <div class="col s12 m12 l12 leave-form">
					    		
					    		<label>Message</label>
					    		<textarea placeholder="Message to approver" name="requestor_message" value="value="{{old('requestor_message')}}""></textarea>
	                    @if ($errors->has('requestor_message'))
                            <span class="help-block">
                                <strong>{{ $errors->first('requestor_message') }}</strong>
                            </span>
                        @endif				    

					    </div>
				

				</form>

					    <div class="col s12 m12 l12 leave-form" style="color: black;">
					    	<h5 style="color: white;">Leave Applications</h5>
							  
							    @if(count($leaverecords)==0)
							    	<h5 id="norequest">No leave request.</h5>
							    @endif
							    	<h5 id="norequest"></h5>

							  <ul class="collapsible" data-collapsible="accordion">
							    
							  	@foreach($leaverecords as $leaverecord)



								    <li class="leaverequest" id="leaverequest-{{$leaverecord->id}}">
								      <div class="collapsible-header">
								      	
								      	<table class="table-leave-apply">
								      		<th id="typeofleave-{{$leaverecord->id}}">{{$leaverecord->type_of_leave}}</th>
								      		<th id="startdateedit-{{$leaverecord->id}}" >FROM: {{$leaverecord->start_date}} </th>
								      		<th id="enddateedit-{{$leaverecord->id}}">TO: {{$leaverecord->end_date}} </th>
								      		<th> {{$leaverecord->status}} </th>
								      		<th> {{$leaverecord->id}} </th>
								      		

								    @if($leaverecord->status == "Approved"|| $leaverecord->status == "Denied")
								    		<th><a class="modal-trigger" onclick="confirmssges({{$leaverecord->id}})" href="#deleteconfirmmodal">Remove</a></th>

								    @else
								      		<th> <a class="modal-trigger" onclick="confirmssges({{$leaverecord->id}})" href="#deleteconfirmmodal">Delete</a> 
								      			<a class="modal-trigger" onclick="editrequest({{$leaverecord->id}})" href="#editrequestmodal" style="margin-left: 20px;">Edit</a></th>

								    @endif
								      	</table>

								      </div>
								      <div class="collapsible-body">

								      	<div class="row">

								      		<input id="requestid-edit-{{$leaverecord->id}}" type="hidden" name="requestid" value="{{$leaverecord->id}}">
								      		<div class="col s12 m12 l6 leave-collapsible">
								      			<h6><strong>Reason:</strong>
								      				<span id="reason-edit-{{$leaverecord->id}}" style="padding-left: 50px;">{{$leaverecord->reason}}</span>
								      			</h6>

								      		</div>

								      		<div class="col s12 m12 l6 leave-collapsible">
								      			<h6><strong>Number of days:</strong>
								      				<span id="numofdays-edit-{{$leaverecord->id}}" style="padding-left: 50px;">{{$leaverecord->number_of_days}}</span>
								      			</h6>

								      		</div>

								      		<div class="col s12 m12 l12 leave-collapsible">
								      			<h6><strong>Message to Approver:</strong></h6>
								      			<span id="msgtoapprover-edit-{{$leaverecord->id}}" > {{$leaverecord->requestor_message}} </span>
								      		</div>

								      		<div class="col s12 m12 l6 leave-collapsible">
												<h6><strong>Processed/Approved by:</strong></h6>
												<span id="processedby-edit-{{$leaverecord->id}}">{{$leaverecord->processed_by}}</span>
								      		</div>

								      		<div class="col s12 m12 l6 leave-collapsible">
												<h6><strong>Processed On:</strong></h6>
												<span id="processedon-edit-{{$leaverecord->id}}">{{$leaverecord->processed_date}}</span>
								      		</div>

								      		<div class="col s12 m12 l12 leave-collapsible">
												<h6><strong>Approver's Remarks:</strong></h6>
												<p id="remarks-edit-{{$leaverecord->id}}">{{$leaverecord->approver_remarks}}</p>
								      		</div>
								      	</div>
								      	
								      </div>
								    
							  	@php
							  		$leaverecordid = $leaverecord->id;
							  	@endphp

								  <!-- Modal Structure -->
								  <div id="deleteconfirmmodal" class="modal">
								    <div class="modal-content">

								    	<h4 id="confirmnote"></h4>
								    	<input type="hidden" name="" id="testid">
								    	
								    </div>
								    <div id="modfooterdelrequest" class="modal-footer">
								      <a class="btn" onclick="delete_request({{$leaverecord->id}})" class="modal-close waves-effect waves-green btn-flat">Proceed</a>
								      <a class="btn" onclick="cancel()" class="modal-close waves-effect waves-green btn-flat">Cancel</a>


								    </div>
				
								  </div>


								  </li>
								
								@endforeach
							  </ul>

						</div>
				    </div>
		        </div>

		      </div>

		  </div>

	@php



	@endphp




	      <div class="col s12 m10 l3">
		      <div class="card">
		        <div class="card-content white-text remaining-leave">

		        	<div class="">
		        		<canvas id="ApproveLeaveChart"></canvas>
		        	</div>

		        </div>

		      </div>

		      <div class="card">
		        <div class="card-content white-text remaining-leave">

		        	<div class="">
		        		<canvas id="SickLeaveChart"></canvas>
		        	</div>

		        </div>

		      </div>

			<div data-tockify-component="mini" data-tockify-calendar="jmarvin"></div>

		  </div>

	      <div class="col s12 m1 l3">

	      </div>
	</div>





  <!-- Modal Structure -->
  <div id="editrequestmodal" class="modal">
    <div class="modal-content">

    	<div class="row">
			<div class="col s12 m8 l9">
				<h5>Leave Request</h5>
			</div>
				{{-- <form method="POST" action="{{route('applyleave')}}"> --}}
					
				      <div class="col s12 m4 l3" >
				      	<button onclick="saveeditrequest()" class="btn approval-btn" style="width: 50%; height: 100%; font-size: 12px; padding: 0px; float: right;" >Save</button>
				      </div>
				      <input type="hidden" name="_token" value=" {{csrf_token()}} ">

				      	<input id="requestidedit" type="hidden" name="requestid" value="">
			        	<div class="col s12 m12 l12 leave-form-edit">
					        <label class="label-edit">Type of Leave</label>
					        
					        <select id="typeofleave_edit" name="type_of_leave">
					        	<option selected disabled>Select leave type</option>
					        	<option value="Vacation">Vacation</option>
					        	<option value="Paternity">Paternity</option>
					        	<option value="Solo Parent">Solo Parent</option>
					        	<option value="Bereavement">Bereavement</option>
					        	<option value="Leave of Absence">Leave of Absence</option>

					        </select>
					    </div>

					    <div class="col s12 m12 l6 leave-form-edit">
					    		<label class="label-edit">Start Date</label>
					        	<input id="startdateedit" class="datepick" type="date" name="startdate">
					    </div>

					    <div class="col s12 m12 l6 leave-form-edit">
					    		
					    		<label class="label-edit">End Date</label>
					        	<input id="enddateedit" class="datepick" type="date" name="enddate">

					    </div>

					    <div class="col s12 m12 l6 leave-form-edit">
					    		
					    		<label class="label-edit">Number of Days</label>
					        	<input id="numbeofdaysedit" class="datepick" type="number" name="numofdays" min="0" value="0">

					    </div>

					    <div class="col s12 m12 l6 leave-form-edit">
					    		
					    		<label class="label-edit">Reason</label>

						        <select id="reasonedit" name="reason">
						        	<option selected disabled>Select reason</option>
						        	<option value="Leisure">Leisure</option>
						        	<option value="Study">Study</option>
						        	<option value="Process Paperworks">Process Paperworks</option>
						        	<option value="Others">Others</option>

						        </select>

					    </div>

					    <div class="col s12 m12 l12 leave-form-edit">
					    		
					    		<label class="label-edit">Message</label>
					    		<textarea id="mssgetoapprover" placeholder="Message to approver" name="requestor_message"></textarea>
					    </div>
				

				{{-- </form> --}}

		</div>
    </div>

  </div>


<script type="text/javascript">

	var SickLeaveChart = document.getElementById('SickLeaveChart').getContext('2d');
	var leaveCount = 2;
	var leaveAvailable = 15 - {{$leaves_usage}}
	// Chart.defaults.global.defaultFontFamily ='Lato';
	// Chart.defaults.global.defaultFontSize = 10;
	Chart.defaults.global.defaultFontColor = '#777';


	// if ({{$leaves_usage}} > 15) {
	// var leaveAvailable = 0;
	// }

	var massPopChart = new Chart(SickLeaveChart,{
		type: 'doughnut', //bar, horizontalbar, pie, line, doughnut
		data:{
			labels:['Used Planned Leave','Leave Available'],
			datasets:[{
				label:'Planned Leave Usage',
				data:[
					leaveCount,
					leaveAvailable,

					],
					// backgroundColor:'green'
					backgroundColor:[
						'rgba(254, 39, 35, 0.6)',
						'rgba(74, 62, 345, 0.6)',
					],

					borderWidth:1,
					borderColor:'#777',
					hoverBorderWidth: 3,
					hoverBorderColor:'#000'

			}],

		},
		options:{
			title:{
				display:true,
				text:'Sick Leave Credits',
				fontSize:15
			},

			legend:{
				display:true,
				position:'right',
				labels:{
					fontColor:'#000',
					fontSize:10
				}
			},
			layout:{
				padding:{
					left:0,
					right:0,
					bottom:0,
					top:0
				}
			},
			tooltips:{
				enabled:true
			}
		}

	});



	var ApproveLeaveChart = document.getElementById('ApproveLeaveChart').getContext('2d');
	var leaveCount = {{$leaves_usage}};
	var leaveAvailable = 15 - {{$leaves_usage}}
	// Chart.defaults.global.defaultFontFamily ='Lato';
	// Chart.defaults.global.defaultFontSize = 10;
	Chart.defaults.global.defaultFontColor = '#777';


	if ({{$leaves_usage}} > 15) {
	var leaveAvailable = 0;
	}

	var massPopChart = new Chart(ApproveLeaveChart,{
		type: 'doughnut', //bar, horizontalbar, pie, line, doughnut
		data:{
			labels:['Used Planned Leave','Leave Available'],
			datasets:[{
				label:'Planned Leave Usage',
				data:[
					leaveCount,
					leaveAvailable,

					],
					// backgroundColor:'green'
					backgroundColor:[
						'rgba(255, 99, 132, 0.6)',
						'rgba(54, 162, 235, 0.6)',
					],

					borderWidth:1,
					borderColor:'#777',
					hoverBorderWidth: 3,
					hoverBorderColor:'#000'

			}],

		},
		options:{
			title:{
				display:true,
				text:'Planned Leave Credits',
				fontSize:15
			},

			legend:{
				display:true,
				position:'right',
				labels:{
					fontColor:'#000',
					fontSize:10
				}
			},
			layout:{
				padding:{
					left:0,
					right:0,
					bottom:0,
					top:0
				}
			},
			tooltips:{
				enabled:true
			}
		}

	});


</script>

@endsection