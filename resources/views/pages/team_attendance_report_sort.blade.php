 @extends('layouts.main_layout-admin')

@section('content')



	    <div class="row ">
	      <div class="col s12 m4 l9">
		      <div id="team-attendanceinfo" class="card " style="background-color: #DCFDFC">
		        <div class="card-content ">

				<h5>Team Attendance Report</h5>
		        <table>
		        	<thead>
			        	<th>Date</th>
			        	<th>Name</th>
			        	<th>Supervisor</th>
			        	<th>Schedule</th>
			        	<th>Log In</th>
			        	<th>Log Out</th>
			        	<th>Status</th>
			        	<th>Tardiness</th>
		        	</thead>

		        	<tbody>
				
				@php
				use App\User;
				use App\EmployeeDetails;
				use App\Personal_info;
				use App\UserAttendanceInfo;
				use App\ShiftSchedule;
				use Carbon\Carbon;
				@endphp		    

		    	@foreach ($loginfo as $log)

		    		@php

		    		$person = Personal_info::where('id',$log->person_id)->first();
					
					$emp_id = User::where('employee_id', $log->id)->first();
					

					$sup_empid = EmployeeDetails::where('id',$log->reports_to)->first();
					$supsearch = Personal_info::where('id', $sup_empid['person_id'])->first();
					$supname = $supsearch['first_name']." ".$supsearch['last_name'];

					if($from == null || $to == null){

						$attendanceinfo = UserAttendanceInfo::where('user_id', $emp_id['id'])
										->get();

					} else {

						$attendanceinfo = UserAttendanceInfo::where('user_id', $emp_id['id'])
										->whereBetween('date', [min($from, $to), max($from, $to)])
										->orderBy('date', 'DESC')
										->get();

					}

					
					
					$logdetails = $attendanceinfo;
					
					$shift = ShiftSchedule::where('shift',$emp_id['shift_schedule'])->first();
					
					$phpdate_starttime = strtotime($shift['start_time']);
					$converted_starttime = date('h:i:s A', $phpdate_starttime);

					$phpdate_endtime = strtotime($shift['end_time']);
					$converted_endtime = date('h:i:s A', $phpdate_endtime);
					


					@endphp 
				
					@foreach ($logdetails as $logdetail)

					@php

						$phpdate = strtotime($logdetail->date);
						$converted_date = date('m/d/y', $phpdate);

						$timein = Carbon::parse($logdetail->log_in);
						$phpdate = strtotime($timein);
						$converted_timein = date('h:i:s A', $phpdate);						
						
						$timeout = Carbon::parse($logdetail->log_out);
						$phpdate = strtotime($timeout);
						$converted_timeout = date('h:i:s A', $phpdate);

						$actualtimein = Carbon::parse($logdetail->log_in);
					    $schedule = Carbon::parse($shift->start_time);

					    $diff_in_seconds = $schedule->diffInSeconds($actualtimein);
					  	$hours = floor($diff_in_seconds / 3600);
					  	$minutes = floor(($diff_in_seconds / 60) % 60);
					  	$seconds = $diff_in_seconds % 60;


						$duration = $hours.":".$minutes.":".$seconds;
						$phpdate = strtotime($duration);
						$convert = date('H:i:s', $phpdate);


						// $asdf = [
						// 	`

						// 		<td style="color: #5CBCB8">On Time</td>
						// 		<td>00:00:00</td>
						// 	`
						// ];

						// echo 
						
					@endphp
		        		<tr>

		        			@if($supsearch['id'] == $supervisor) 

		        				<td> {{$converted_date}} </td>
				        		<td> {{$person->first_name}} {{$person->last_name}} </td>
				        		<td> {{$supname}}</td>
				        		<td> {{$converted_starttime}} - {{$converted_endtime}}</td>
				        		<td> {{$converted_timein}} </td>
				        		<td> {{$converted_timeout}} </td>
				        		
								@if ($actualtimein < $schedule) 

									<td style="color: #5CBCB8">On Time</td>
									<td>00:00:00</td>
								@else
									<td style="color: #EF5B5B">Late</td>
									<td style="color: #EF5B5B">{{$convert}}</td>
								@endif	        		


		        			@endif

			        		
		        		
		        		
		        		</tr>
		        	@endforeach
		        @endforeach

		        	</tbody>
		        </table>


		        </div>

		      </div>
	
		  </div>
	      <div class="col s12 m4 l3">
		      <div class="card blue-grey darken-1">
		        <div id="filters" class="card-content white-text" style="height: 600px;">

		        	<form method="GET" action="/team_attendance_report/sort">
		        		@csrf
		        		<label>Filter By Date</label>
			        	<div class="row">
			        		<div id="filterdate1" class="col s12 m4 l6 ">
					        	<label>Start</label>
					        	<input id="filterdatefrom" type="text" name="from" placeholder="mm/dd/yyyy" required>
			        		</div>

			        		<div id="filterdate2" class="col s12 m4 l6 filters">
			        			<label>End</label>
			        			<input id="filterdateto" type="text" name="to" placeholder="mm/dd/yyyy" disabled required>
			        		</div>
			        	</div>

			        	<label>Filter By Team Supervisor</label>

	    					<select id="reports_to" name="reports_to" style="display: block;" value="{{old('reports_to')}}" required>


	    						@foreach ($teamleaders as $teamleader) 
	    							@php
										$person =  $teamleader->person_id;
										$test = Personal_info::where('id', $person)->first();
									@endphp
									
									<option id="emp-{{$teamleader->id}}" value="{{$teamleader->id}}" > 
										{{$test->first_name}} {{$test->last_name}}
									</option>
	    						@endforeach

	    					</select>


						<button class="btn" style="margin: 0 auto; width: 100%; font-size: 10px; margin-top: 30px;" {{-- onclick="filter()" --}} >Apply Filter</button>		

		        	</form>
		        	        	
		        </div>

		      </div>
	

		  </div>

	      <div class="col s12 m4 l9">
		      <div id="team-attendanceinfo" class="card " style="background-color: #DCFDFC">
		        <div class="card-content ">

				<h5>Team Attendance Report</h5>
		        <table id="myTable">
		        	<thead>
			        	<th>Date</th>
			        	<th>Name</th>
			        	<th>Supervisor</th>
			        	<th>Schedule</th>
			        	<th>Log In</th>
			        	<th>Log Out</th>
			        	<th>Status</th>
			        	<th>Tardiness</th>
		        	</thead>

		        	<tbody id="mainbody">


		        	</tbody>
		        </table>


		        </div>

		      </div>
	
		  </div>


	    </div>

<tr style="display: hidden;"></tr>

<script type="text/javascript">



  $( function() {
    $( "#filterdatefrom" ).datepicker();
    $( "#filterdateto" ).datepicker();
  } );


   	$('#filterdatefrom').change( function() {
   		if ($('#filterdatefrom').val().length != 0) {
   			$('#filterdateto').removeAttr('disabled');
   		}
  	});



function filter(){

  const filterdatefrom = $('#filterdatefrom').val();
  const filterdateto = $('#filterdateto').val();
  const reports_to = $('#reports_to').val();

  const splitfromdate = filterdatefrom.split("/");
  const fromdate = splitfromdate[0]+splitfromdate[1]+splitfromdate[2]

  const splitdateto = filterdateto.split("/");
  const todate = splitdateto[0]+splitdateto[1]+splitdateto[2]


  const dates = document.getElementById("mainbody").querySelectorAll("tr"); 


    $.ajax({
      url: '/ajxfilter',
      method: 'get',
      data: {'filterdatefrom':filterdatefrom, 'filterdateto':filterdateto, 'supervisor':reports_to}
      }).done( function(data){
          

      	$('#mainbody').html(data);
      	alert(data);
    	
    });x


  // for(var date of dates ){

  // 	if (date.className >= 'date-'+fromdate && date.className <='date-'+todate){


  // 	  date.style.display = '';


  // 	}else{

  // 		date.style.display = 'none';
  // 	}
 
  // }










}


</script>
@endsection