@extends('layouts.main_layout-nosidebar')

@section('content')
	

    <div class="container-test">
	    <div class="row">

	      <div class="col s12 m12 l3" id="emp-profile-card">
	
		  </div>

	      <div class="col s12 m12 l6" id="emp-profile-card">
		      <div class="card blue-grey darken-1 ">
		        <div class="card-content white-text thumbnail">

					<div id="profile-photo">
						<img src="images/{{$users_admin->avatar}}">
						<p><a href="#modal1" class="modal-trigger">Change Image</a></p>
					</div>

		        </div>

		      </div>
	
		  </div>

	      <div class="col s12 m12 l3" id="emp-profile-card">
	
		  </div>

	    </div>

	    <div class="row">

	      <div class="col s12 m12 l3">
	
		  </div>

	      <div class="col s12 m12 l6">
		      

		        <div class="card blue-grey darken-1 emp-profile-card-info" style="border-top: 1px solid white;">
		        	<div class="card-content white-text">


				      <div class="row">

				        <div class="col s12 m6 l6">
				        	<h5 class="personal-data-header">Personal Details</h5>
					        <div class="row">
						        <div class=" col s6 m6 l6">

					        		<ul >
					        			<li class="personal-data-label">First Name</li>
					        			<li class="personal-data-label">Last Name</li>
					        			<li class="personal-data-label">Date of Birth</li>
					        			<li class="personal-data-label">Marital Status</li>
					        			<li class="personal-data-label">Email Address</li>
					        		</ul>
					        	</div>


					        	@php
					        		$phpdate_birthday = strtotime($person->birthday);
									$birthday = date('F j, Y', $phpdate_birthday);
					        	@endphp
					        	<div class=" col s6 m6 l6">
					        		<ul >
					        			<li class="personal-data-profile"> {{$person->first_name}} </li>
					        			<li class="personal-data-profile"> {{$person->last_name}} </li>
					        			<li class="personal-data-profile">{{$birthday}}</li>
					        			<li class="personal-data-profile">{{$person->status}}</li>
					        			<li class="personal-data-profile">{{$person->email}}</li>
					        		</ul>

					        	</div>
				        	</div>
				        </div>
				        	
				        <div class="col s12 m6 l6">
				        	<h5 class="personal-data-header">Employment Details</h5>
					        <div class="row">			        
					        	<div class="col s6 m6 l6">
					        		
					        		<ul>
					        			<li></li>
					        			<li class="personal-data-label">Employee ID</li>
					        			<li class="personal-data-label">Position</li>
					        			<li class="personal-data-label">Department</li>
					        			<li class="personal-data-label">Location</li>
					        			<li class="personal-data-label">Reporting To</li>
					        			<li class="personal-data-label">Joining Date</li>
					        			<li class="personal-data-label">Employment Status</li>
					        			
					        		</ul>

					        	</div>

					        	@php
					        		$phpdate_joiningdate = strtotime($employees->joining_date);
									$joining_date = date('F j, Y', $phpdate_joiningdate);
					        	@endphp

					        	<div class="col s6 m6 l6">

					        		<ul>
					        			<li class="personal-data-profile"> {{$employees->id}} </li>
					        			<li class="personal-data-profile"> {{$employees->position}}</li>
					        			<li class="personal-data-profile">{{$employees->department}}</li>
					        			<li class="personal-data-profile">{{$employees->location}}</li>
					        			<li class="personal-data-profile"> {{$sup->first_name}} {{$sup->last_name}}</li>
					        			<li class="personal-data-profile">{{$joining_date}}</li>
					        			<li class="personal-data-profile">{{$employees->employment_status}}</li>
					        		</ul>

				        		</div>


	
		      				</div>
		        
		        		</div>
		      </div>


	    </div>
    </div>
    </div>
    </div>



  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
	    <div class="row">

	      <div class="col s12 m12 l6">
		      <div class="">

		      		<img src="images/{{$users_admin->avatar}}" style="height: 100%;width: 100%;">
		      </div>

		  </div>
	      <div class="col s12 m12 l6">

		        <div class="">
		      		
		      		<h5>Change Profile Image</h5>

					<form enctype="multipart/form-data" action="/updateprofile" method="POST">
						<label>Update Profile Image</label><br>
						<input type="file" name="avatar"><br><br>
						<input type="hidden" name="_token" value=" {{csrf_token()}} ">
						<input type="submit" name="" class="btn" style="height: 100%;width: 100%;">
					</form>

		   

		      </div>
		      </div>
	
		  </div>
	    </div>
    </div>

  </div>
@endsection

