@extends('layouts.main_layout-admin')

@section('content')

{{csrf_field()}}


	
<div class="row">
	<div class="col s12 m12 l12">
	<div class="card blue-grey darken-1">
		<div class="card-content">
		<h5 style="color: white;">Leave Requests</h5>
	  <ul class="collapsible popout leavelist" data-collapsible="accordion">
	  	
		@php
		 	use App\Personal_info;
			use App\User;
			use App\EmployeeDetails;

		@endphp

		@if(count($leaverecords) == 0)
			<span id="noleaverequestnote" style="color: white; font-size: 30px;">No leave request in queue.</span>
		@endif
			<span id="noleaverequestnote" style="color: white; font-size: 30px;"></span>
	  	@foreach($leaverecords as $leaverecord)

	  	@php

	  		$user_id = User::where('id',$leaverecord->user_id)->first();
	  		$employee_id = $user_id->employee_id;
	  		$person_id = EmployeeDetails::where('id',$employee_id)->first();
	  		$person = Personal_info::where('id',$person_id->person_id)->first();
	  		
	  	@endphp

	  		
		  	<li class="leaveitems" id="leaveitemno-{{$leaverecord->id}}">
				<div class="collapsible-header">
			    	<table class="tl-leave-inbox">

			    		
			    			<th>{{$person->first_name}} {{$person->last_name}}</th>
			    			<th>{{$leaverecord->type_of_leave}}</th>
			    			<th>{{$leaverecord->start_date}}</th>
			    			<th>{{$leaverecord->end_date}}</th>
			    			<th>
			    				<a class="modal-trigger" onclick="approve({{$leaverecord->id}})" id="approve-{{$leaverecord->id}}" href="#modal1">Approve</a>
			    				<a class="modal-trigger" onclick="deny({{$leaverecord->id}})" id="deny-{{$leaverecord->id}}" href="#modal1">Deny</a>
			    			</th>	    			
			    

			    	</table>
		        </div>
		        <div class="collapsible-body">

				      	<div class="row">

				      		<div class="col s12 m4 l6 leave-collapsible">
				      			<h6><strong>Request Id No.:</strong><span style="padding-left: 50px;">
				      				{{$leaverecord->id}}</span></h6>

				      		</div>

				      		<div class="col s12 m4 l6 leave-collapsible">
				      			<h6><strong>Reason:</strong><span style="padding-left: 50px;">
				      				{{$leaverecord->reason}}</span></h6>

				      		</div>

				      		<div class="col s12 m4 l6 leave-collapsible">
				      			<h6><strong>Number of days:</strong><span style="padding-left: 50px;">
				      				{{$leaverecord->number_of_days}}</span></h6>
				      		</div>

				      		<div class="col s12 m4 l12 leave-collapsible">
				      			<h6><strong>Requestor's Message:</strong></h6>
				      			<span> {{$leaverecord->requestor_message}} </span>
				      		</div>


				      		<div class="col s12 m4 l12 leave-collapsible">

								<input type="hidden" name="_token" value="{{csrf_token()}}">

								<h6><strong>Remarks:</strong></h6>
								<textarea style="height: 150px;" placeholder="Message to the requestor" id="approvers_message-{{$leaverecord->id}}"></textarea>
				      		</div>
				      	</div>

		        </div>
		    </li>
	
		@endforeach

	  </ul>
	  </div>
	</div>        
	</div>
</div>



  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h6 id="modal-notif"></h6>

    </div>

  </div>



@endsection
