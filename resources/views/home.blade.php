@extends('layouts.main_layout')

@section('content')
    
    <!-- Navbar goes here -->

    <!-- Page Layout here -->

              <div class="row">
                <div class="col s12 m12 l12">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <div class="row">
                    
                    

                            <div class="col s12 m4 l5" style="padding: 8px">
                                <h6> {{$convert_dateToday}} </h6>
                                <h5 id="txt"></h5>


                            </div>
                        

                            <div class="col s12 m4 l4" style="padding: 8px">
                                <h6> {{$employeedepartment}} </h6>
                                <p> {{$schedule}} </p>
                 

                            </div>

                            <div class="col s12 m4 l3">

                                <a href="/clockin"id="clockin" class="btn">Clock In</a>
                                {{-- <button  class="btn" ></button> --}}

                                <a href="/clockout"id="clockout" class="btn">Clock Out</a>                            

                            </div>

                        </div>
                    </div>
    
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col s12 m12 l12">
                  <div class="card blue-grey darken-1">
                    <div class="card-content white-text">

                        <h5>My Attendance Data</h5>
                        <table id="table-login">
                            <thead id="loginhead">
                                <th>Date</th>
                                <th>Log In</th>
                                <th>Log Out</th>
                                <th>Status</th>
                                <th>Tardiness</th>
                            </thead>

                    @php
                        use App\User;
                        use App\EmployeeDetails;
                        use App\Personal_info;
                        use App\UserAttendanceInfo;
                        use App\ShiftSchedule;
                        use Carbon\Carbon;



                    @endphp 
                            
                            <tbody id="loginbody">


                                @foreach($userlogs as $log)

                                    @php
                                        $phpdate = strtotime($log->date);
                                        $converted_date = date('d-M-Y', $phpdate);

                                        $phpdate_clockin = strtotime($log->log_in);
                                        $converted_clockin = date('h:i:s A', $phpdate_clockin);


                                        if ($log->log_out == null) {
                                            $converted_clockout = "";
                                        }else{
                                             $phpdate_clockout = strtotime($log->log_out);
                                             $converted_clockout = date('h:i:s A', $phpdate_clockout);
                                        }
                                        

                                    @endphp

                                    <tr class="loginrow">
                                        <td class="logdata">{{$converted_date}}</td>       
                                        <td class="logdata">{{$converted_clockin}}</td>       
                                        <td class="logdata">{{$converted_clockout}}</td>      
                                        <td class="logdata">{{$log->status}}</td>       
                                        <td class="logdata">{{$log->tardiness}}</td>       
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
    
                  </div>
                </div>
              </div>

@endsection