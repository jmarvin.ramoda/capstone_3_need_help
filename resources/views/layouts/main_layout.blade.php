<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
        
        @include('partials.header')
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    </head>
    <body onload="startTime()">



        @include('partials.nav')


    <div class="container-test">
        <div class="row"  id="sidebarrow-user">
          <div class="col s12 m4 l2" id="sidebarlayout-user" > <!-- Note that "m8 l9" was added -->
              

              <div class="row" >
                <div class="col s12 m12 l12" id="home-bar-user" >
                  <div class="card" id="left-home" >
                    <div class="card-content white-text thumbnail-home">

                        <div id="profile-pic">
                            <img class="" src="/images/{{$users->avatar}}">
                        </div>
                        <div id="emp-name">
                            <h5> {{$users->username}} </h5>
                            <h6> {{$employeeposition}} </h6>

                        </div>
                        <div>
                            <h6>My Tools</h6><hr>
                            <ul>
                                <li class="sidenavlink"><a href="/dashboard">Dashboard</a></li>
                                <li class="sidenavlink"><a href="/home">Attendance</a></li>                               
                                <li class="sidenavlink"><a href="/applyleave">Leaves</a></li>
                            </ul>
                        </div>
                    </div>
                  </div>
    
                  </div>


                </div>
              </div>


          <div class="col s12 m12 l10"> <!-- Note that "m4 l3" was added -->

                 @yield('content')
          </div>


          </div>

    </div>


       
        

        @include('partials.footer')

          
    </body>
</html>
