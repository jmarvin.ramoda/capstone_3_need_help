<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
        
        @include('partials.header')

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <style>


        </style>
    </head>
    <body>

        <div class="row">
            <div class="col s12 m6 l6" id="left-body-login">

                <div class="row">
                    <div class="col s12 m12 l12" id="companylogo">
                        <img src="images/4ea472f4-23ce-48e8-8b95-1a050c12b569.png">

                    </div>
                </div>

                @yield('content')


            </div>

            <div class="col s12 m6 l6" id="right-body-login">

                

            </div>

        </div>
        
    </body>
</html>
