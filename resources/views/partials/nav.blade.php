<nav class="nav-template">
  <div class="nav-wrapper"  id="thumbnail-logo">
    <a href="/home" class="brand-logo" id="logoimage"><img src="/images/imageedit_6_7705082539.png" ></a>
    <ul id="nav-mobile" class="right hide-on-med-and-down">
	<!-- Authentication Links -->
	@guest
	    <li><a href="{{ route('login') }}">Login</a></li>
	    <li><a href="{{ route('register') }}">Register</a></li>
	@else
      	<li class="dropdown">
		    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
		        {{ Auth::user()->name }} <span class="caret"></span>
		    </a>

		    <ul class="dropdown-menu">

		    	<li><a href="/myprofile">My Profile</a></li>
		        <li>
		            <a href="{{ route('logout') }}"
		                onclick="event.preventDefault();
		                         document.getElementById('logout-form').submit();">
		                Logout
		            </a>

		            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		                {{ csrf_field() }}
		            </form>
		        </li>
		    </ul>
		</li>
	@endguest
    </ul>

	<a href="#" data-target="slide-out" class="sidenav-trigger" id="sidenav-trigger"><i class="fas fa-bars"></i></a>
  </div>
</nav>


<ul id="slide-out" class="sidenav">
    <li><div class="user-view">
{{--       <div class="background">
        		<img src="images/office.jpg">
      	   </div> --}}
      <a href="#user"><img id="sidenavpic-user" class="circle" src="/images/{{$users->avatar}}"></a>
    </div>

    <div id="emp-name">
        <h5> {{$users->username}} </h5>
        <h6> {{$employeeposition}} </h6>

    </div><hr>
    </li>
    <li class="sidenavlink"><a href="/dashboard">Dashboard</a></li>
    <li class="sidenavlink"><a href="/home">Attendance</a></li>                               
    <li class="sidenavlink"><a href="/applyleave">Leaves</a></li>
    <li><a href="/myprofile">My Profile</a></li>
    <li>
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
  </ul>

