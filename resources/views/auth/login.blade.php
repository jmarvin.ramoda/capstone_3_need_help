@extends('layouts.app-2')

@section('content')

        <div class="box">

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong class="errmsg">{{ $errors->first('username') }}</strong>
                    </span>
                @endif
          <h4>Login</h4>
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
              
              <div id="topinputbox" class="inputBox {{ $errors->has('username') ? ' has-error' : '' }}">
                <input type="text" name="username" required="" value="{{ old('username') }}">
                <label>Username</label>



              </div>
              <div class="inputBox {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" name="password" required="" value="{{ old('password') }}">
                <label>password</label>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong class="errmsg">{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                
              </div>

              <input id="loginbutton" type="submit" name="" value="Submit" required=""><br><br>

              <a style="color: white; text-decoration: none; text-align: right;" class="" href="{{ route('password.request') }}">
                  Forgot Your Password?
              </a>
          </form>
        </div>
@endsection
                      