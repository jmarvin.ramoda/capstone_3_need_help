@extends('layouts.app-2')

@section('content')

<div class="box">

        @if ($errors->has('username'))
            <span class="help-block">
                <strong class="errmsg">{{ $errors->first('username') }}</strong>
            </span>
        @endif
          <h2 style="margin-bottom: 30px;">Reset Password</h2>
            
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                 {{ csrf_field() }}
              <div class="inputBox {{ $errors->has('username') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>f
                    
                @endif

              </div>

              <input type="submit" name="" value="Send Password Reset Link" required=""><br><br>
          </form>
        </div>


@endsection
