@extends('layouts.main_layout-admin	')

@section('content')

<!-- 	@if (count($errors) > 0)
		@foreach ($errors->all() as $error)
			<p class="alert alert-danger">{{$error}}</p>
		@endforeach

	@endif -->

    <form class="form-horizontal" method="POST" action="{{route('custom.register')}}">
    	{{ csrf_field() }}
			  
			  <div class="row">
		      <div class="col s12 m4 l8">
		      
			      <div class="card blue-grey darken-1">
			        
				        <div class="card-content white-text">

				        	<div class="row">

				        		<div class="col s12 m4 l12">
				        			<h5>Personal Information</h5>
						        	<div class="row">
						        		
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>First Name</label>
						        			<input class="registerbyadmininput" type="text" name="fname" value="{{old('fname')}}">


	                                @if ($errors->has('fname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('fname') }}</strong>
	                                    </span>
	                                @endif

						        		</div>
						       
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Last Name</label>
				        					<input class="registerbyadmininput" type="text" name="lname" value="{{old('lname')}}">

	                                @if ($errors->has('lname'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('lname') }}</strong>
	                                    </span>
	                                @endif

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Birthday</label>
				        					<input class="registerbyadmininput" type="date" name="dob" value="{{old('dob')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Marital Status</label>
				        					<input class="registerbyadmininput" type="text" name="mstatus" value="{{old('mstatus')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Email Address</label>
				        					<input class="registerbyadmininput" type="text" name="email" value="{{old('email')}}">

						        		</div>

						        	</div>

				        			<h5>Employment Information</h5>
						        	<div class="row">
						        		
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Designation</label>
						        			<input class="registerbyadmininput" type="text" name="position" value="{{old('position')}}">
						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Department</label>
				        					<input class="registerbyadmininput" type="text" name="department" value="{{old('department')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Location</label>
				        					<input class="registerbyadmininput" type="text" name="location" value="{{old('location')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Position/Rank</label>
						        			<select style="display: block;" name="rank" value="{{old('rank')}}">

						        				<option disabled selected>Select Rank</option>
						        				<option>Team Member</option>
						        				<option>Supervisor</option>
						        				<option>Manager</option>
						        				<option>Director</option>
						        			</select>
						        		</div>

						       
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Reports To</label>

				        					<select name="reports_to" style="display: block;" value="{{old('reports_to')}}">

				        						<option disabled selected>Select Supervisor Name</option>
			
				        						@php
		        								 use App\Personal_info;

		        								@endphp

				        						@foreach ($teamleaders as $teamleader) 
				        							@php
														$person =  $teamleader->person_id;
														$test = Personal_info::where('id', $person)->first();
													@endphp
													
													<option id="emp-{{$teamleader->id}}" value="{{$teamleader->id}}"> 
														{{$test->first_name}} {{$test->last_name}}
													</option>
				        						@endforeach
				        					</select>

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Joining Date</label>
				        					<input class="registerbyadmininput" type="date" name="joindate" value="{{old('joindate')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Employment Status</label>
						        			<select class="registerbyadmininput" style="display: block; color: black" name="empstatus" value="{{old('empstatus')}}">

						        				<option disabled selected>Select Employment Status</option>
						        				<option>Permanent</option>
						        				<option>Probationary</option>
						        				<option>Project Based</option>
						        			</select>
		

						        		</div>


						        	</div>
				        			
				        			<h5>User Log In (Temporary)</h5>
						        	<div class="row">
						        		
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Username</label>
						        			<input class="registerbyadmininput" type="text" name="username" value="{{old('username')}}">
						        		</div>
						       
						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Password</label>
				        					<input class="registerbyadmininput" type="text" name="password" value="{{old('password')}}">

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>Shift Schedule</label>
						        			<select style="display: block; color: black" name="shift"> {{-- shift --}}

						        				@foreach($schedules as $schedule)
						        					<option value="{{$schedule->shift}}">{{$schedule->shift}} {{$schedule->start_time}} - {{$schedule->end_time}}</option>
						        				@endforeach
						        			</select><br>
				        					

						        		</div>

						        		<div class="col s12 m4 l6 registerbyadmin">
						        			<label>User Type</label>
						        			<select style="display: block; color: black" name="user_type">
						        				<option value="1">Admin</option>
						        				<option value="2">Basic User</option>
						        			</select><br>
				        					

						        		</div>


						        		<div class="col s12 m4 l12" style="margin: 0 auto;">
											<button class="btn" name="action" type="submit" style="margin: ">Add Employee</button>

						        		</div>

						        	</div>


				        		</div>
				        	</div>

				        </div>

			      </div>
	
		  	  </div>
		  	  </div>

	</form>


@endsection