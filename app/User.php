<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{   

    use SoftDeletes;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','password'
    ];
    protected $table = 'users';
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function UserLogs(){
        return $this->hasMany('App\UserAttendanceInfo');
    }

    public function LeaveRequests(){
        return $this->hasMany('App\ApplyLeave');
    }


    public function Employee_Details(){
        return $this->belongsTo('App\EmployeeDetails');
    }

    public function Shift_schedule(){
        return $this->belongsTo('App\ShiftSchedule');
    }



    protected $dates = ['deleted_at'];
}



