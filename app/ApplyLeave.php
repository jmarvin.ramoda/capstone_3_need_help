<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class ApplyLeave extends Model
{   
    use SoftDeletes;

    protected $table = 'leave_statuses';
	   public $timestamps = false;


    public function user(){
    	return $this->belongsTo('App\User');
    }

  public static function updateData($leaveId, $message, $approvers_name, $realtimeDate,$status){
    DB::table('leave_statuses')
    ->where('id', $leaveId)
    ->update(['approver_remarks' => $message,'status' => $status, 'processed_by' => $approvers_name, 'processed_date' => $realtimeDate]);
  }

  protected $dates = ['deleted_at'];
}
