<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmployeeDetails extends Model
{	

	use SoftDeletes;

    protected $table = 'employees';
	public $timestamps = false;


    public function persondetails(){
    	return $this->belongsTo('App\Personal_info');
    }

    public function User_Logs(){
        return $this->hasMany('App\User');
    }

    protected $dates = ['deleted_at'];

}
