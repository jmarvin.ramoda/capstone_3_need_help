<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    
function handle($request, Closure $next){
    if (Auth::check() && Auth::user()->role_id == 1) {
        return $next($request);
    }
    elseif (Auth::check() && Auth::user()->role_id == 2) {
        return redirect('/home');
    }

}
}
