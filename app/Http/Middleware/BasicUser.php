<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BasicUser
{
 
function handle($request, Closure $next)
{
    if (Auth::check() && Auth::user()->role_id == 2) {
        return $next($request);
    }
    elseif (Auth::check() && Auth::user()->role == 1) {
        return redirect('/tlhomepage');
    }

}
}
