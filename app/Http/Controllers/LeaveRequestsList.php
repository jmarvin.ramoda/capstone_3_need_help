<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;
use App\ApplyLeave;
use App\Personal_info;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LeaveRequestsList extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }	

   	function index(){

    $user_id_admin = Auth::user();
    $users_admin = User::find($user_id_admin->id);
    $test = $user_id_admin->employee_id;
    $employeeid = EmployeeDetails::find($test);
    $personinfo = Personal_info::find($employeeid)->first();
    $employeedepartment = $employeeid->department;
    $employeeposition = $employeeid->position;

 	$leaverecords = ApplyLeave::where(['supervisor_id' => $employeeid->id,
                                       'status' => 'waiting to be processed'])->get();


	return view('pages.leave_requests_inbox',compact('users_admin','employeedepartment','employeeposition','leaverecords','personinfo'));

	}


    function approve(Request $request){ 

        $leaveId = $request->leaveId;
        $message = $request->message;

        $employeeid = Auth::user()->employee_id;
        $person_id = EmployeeDetails::where('id',$employeeid)->first()->person_id;
        $personname = Personal_info::where('id',$person_id)->first();
        

        $dt = Carbon::now('UTC');
        $realtimeDate = $dt->addHours(8);

        $approvers_name = $personname->first_name." ".$personname->last_name;

        $status = "Approved";

        // echo $realtimeDate;
        ApplyLeave::updateData($leaveId, $message, $approvers_name, $realtimeDate,$status);

        // return redirect('/home');


        // echo "The data has been succesfully passed on.<br>".$message ;
    }


    function deny(Request $request){ 

        $leaveId = $request->leaveId;
        $message = $request->message;

        $employeeid = Auth::user()->employee_id;
        $person_id = EmployeeDetails::where('id',$employeeid)->first()->person_id;
        $personname = Personal_info::where('id',$person_id)->first();
        

        $dt = Carbon::now('UTC');
        $realtimeDate = $dt->addHours(8);

        $approvers_name = $personname->first_name." ".$personname->last_name;

        $status = "Denied";

        // echo $realtimeDate;
        ApplyLeave::updateData($leaveId, $message, $approvers_name, $realtimeDate,$status);

        // return redirect('/home');


        // echo "The data has been succesfully passed on.<br>".$message ;
    }






//load.php

function load(){

$data = array();
$query = DB::table('leave_statuses')
            ->where('status','Approved')
            ->get();



// $result = $statement->$query;

$result = $query;

foreach($result as $row){

$user_id = $row->user_id;
$employee_id = User::where('id',$user_id)->first();
$person_id = EmployeeDetails::where('id',$employee_id->employee_id)->first();
$person = Personal_info::where('id',$person_id->person_id)->first();
$person_name = $person->first_name." ".$person->last_name;

 $data[] = array(
  'id'   => $row->id,
  'title'   => $person_name." - ".$row->reason." ".$row->type_of_leave,
  'start'   => $row->start_date,
  'end'   => $row->end_date
 );


echo json_encode($data);

}






}
}
