<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\EmployeeDetails;
use App\ApplyLeave;
use App\Personal_info;
use Illuminate\Support\Facades\DB;


class LeaveApplicationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $user_id = Auth::user()->id;
        $users = User::find($user_id);
        $test = $users->employee_id;
        $employees = EmployeeDetails::find($test);
        $employeedepartment = $employees->department;
        $employeeposition = $employees->position;
        $leaverecords = ApplyLeave::where('user_id',$user_id)->get();

        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();
        
        $leaves_usage = DB::table('leave_statuses')
                    ->where('user_id',$user_id)
                    ->whereIn('type_of_leave', ['Solo Parent', 'Vacation', 'Paternity','Maternity','Bereavement'])
                    ->sum('number_of_days');                           


        return view('pages.leave_application',compact('leaves_usage','users','employeedepartment','employeeposition','leaverecords','teamleaders'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $this->validation($request);
        $startdateraw = $request->startdate;

        $startdate_res = explode('-', $startdateraw);
        $day = $startdate_res[2];
        $month = $startdate_res[1];
        $year = $startdate_res[0];
        $startdate = $year.'-'.$month.'-'.$day;


        $enddateraw = $request->enddate;
        $enddate_res = explode('-', $enddateraw);
        $day = $enddate_res[2];
        $month = $enddate_res[1];
        $year = $enddate_res[0];

        $enddate = $year.'-'.$month.'-'.$day;

        $user_id = Auth::user()->id;
        $newleaveapp = new ApplyLeave;
        $newleaveapp->type_of_leave = $request->type_of_leave;
        $newleaveapp->start_date = $startdate;
        $newleaveapp->end_date = $enddate;
        $newleaveapp->number_of_days = $request->numofdays;
        $newleaveapp->reason = $request->reason;
        $newleaveapp->requestor_message = $request->requestor_message;
        $newleaveapp->status = "waiting to be processed";
        $newleaveapp->supervisor_id = $request->reports_to;
        $newleaveapp->user_id = $user_id;
        $newleaveapp->save();

        return redirect('/applyleave');

    }

        public function validation($request){
        
        return $request->validate([

            'type_of_leave' => 'required|max:191',
            'startdate' => 'required|date|date_format:Y-m-d|before:end_at',
            'enddate' => 'required|date|date_format:Y-m-d|after:start_at',
            'numofdays' => 'required|min:1',
            'reason' => 'required|max:191',
            'reports_to' => 'required|max:191',
            'requestor_message' => 'required|max:191',

        ]);

    }


        function deleterequest(Request $request){

            $leaverequest = ApplyLeave::find($request->id);
            
            // echo $leaverequest;

            if($leaverequest->delete()){
                echo "Data Deleted";
            }
        }


        function editrequest(Request $request){


        $requestid = $request->requestid;
        $leaverequesteedit = ApplyLeave::find($request->requestid);
        $leaverequesteedit->type_of_leave = $request->type_of_leave;
        $leaverequesteedit->start_date = $request->startdate;
        $leaverequesteedit->end_date = $request->enddate;
        $leaverequesteedit->number_of_days = $request->numofdays;
        $leaverequesteedit->reason = $request->reason;
        $leaverequesteedit->requestor_message = $request->requestor_message;

        $leaverequesteedit->save();

        echo 'Edit Successful';
        }




}

