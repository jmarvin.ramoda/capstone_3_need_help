<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAttendanceInfo;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;
use App\ShiftSchedule;
use Carbon\Carbon;

class BasicUserDashboardController extends Controller
{	

    public function __construct()
    {
        $this->middleware('auth');
    }	

	function index(){
		return view('/home');
	}

	function home(){
	    $dt = Carbon::now('UTC');
    	$realtimeDate = $dt->addHours(8);

    	$phpdate = strtotime($realtimeDate);
        $convert_dateToday = date('l, F, j Y', $phpdate);

		$user_id = Auth::user();
		$users = User::find($user_id->id);
		$test = $users->employee_id;
		$employees = EmployeeDetails::find($test);
		$employeedepartment = $employees->department;
		$employeeposition = $employees->position;
		$userlogs = $users->userlogs;
		
        $shift = ShiftSchedule::where('shift',$users->shift_schedule)->first();
        
        $phpdate = strtotime($shift->start_time);
        $convert_starttime = date('H:i:s A', $phpdate);

        $phpdate = strtotime($shift->end_time);
        $convert_endtime = date('h:i:s A', $phpdate);

		$schedule = $convert_starttime." - ".$convert_endtime;


		return view('/home',compact('convert_dateToday','schedule','userlogs','users','employeedepartment','employeeposition'));

	}



	function dashboardindex(){


 	return view('/dashboard');
	}


}
