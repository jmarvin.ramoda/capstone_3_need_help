<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;

use App\Personal_info;
use App\ApplyLeave;
use Carbon\Carbon;
use App\UserAttendanceInfo;

class DashboardController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }	

	function index(){
    $dt = Carbon::now('UTC');
    $realtimeDate = $dt->addHours(8);

    $phpdate = strtotime($realtimeDate);
    $convert_dateToday = date('Y-m-d', $phpdate);

    $user_id = Auth::user()->id;
    $users = User::find($user_id);
    $test = $users->employee_id;
    $employeeid = EmployeeDetails::find($test);
    $personinfo = Personal_info::find($employeeid)->first();
    $employeedepartment = $employeeid->department;
    $employeeposition = $employeeid->position;
    $leaverecords = ApplyLeave::where('user_id',$user_id)
                              ->where('status','Approved')->get();

    $user_attendance = UserAttendanceInfo::where('user_id', $user_id)
                                         ->where('date', $convert_dateToday)->first();


    $workdays = array();
    $type = CAL_GREGORIAN;
    $month = date('n'); // Month ID, 1 through to 12.
    $year = date('Y'); // Year in 4 digit 2009 format.
    $day_count = cal_days_in_month($type, $month, $year); // Get the amount of days

    $numberofworkingdays = $this->calculateWorkingDaysInMonth($year, $month);

 

	return view('pages.dashboard',compact('numberofworkingdays','user_attendance','users','employeedepartment','employeeposition','leaverecords','personinfo'));
	}



    function calculateWorkingDaysInMonth($year = '', $month = '')
    {
        //in case no values are passed to the function, use the current month and year
        if ($year == '')
        {
            $year = date('Y');
        }
        if ($month == '')
        {
            $month = date('m');
        }   
        //create a start and an end datetime value based on the input year 
        $startdate = strtotime($year . '-' . $month . '-01');
        $enddate = strtotime('+' . (date('t',$startdate) - 1). ' days',$startdate);
        $currentdate = $startdate;
        //get the total number of days in the month 
        $return = intval((date('t',$startdate)),10);
        //loop through the dates, from the start date to the end date
        while ($currentdate <= $enddate)
        {
            //if you encounter a Saturday or Sunday, remove from the total days count
            if ((date('D',$currentdate) == 'Sat') || (date('D',$currentdate) == 'Sun'))
            {
                $return = $return - 1;
            }
            $currentdate = strtotime('+1 day', $currentdate);
        } //end date walk loop
        //return the number of working days
        return $return;
    }

}

    
	