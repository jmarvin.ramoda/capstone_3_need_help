<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\UserAttendanceInfo;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;
use App\Personal_info;
use Image;

class Personal_infoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
	function index(){

		$user_id_admin = Auth::user();
		$users_admin = User::find($user_id_admin->id);
		$emp_id = $users_admin->employee_id;
		$employees = EmployeeDetails::find($emp_id);
		$sup_id = EmployeeDetails::find($employees->reports_to);
		$sup = Personal_info::find($sup_id->person_id);
		$person = Personal_info::where("id",$employees->person_id)->first();
		$employeeposition = $employees->position;


		return view('pages.employee_profile',compact('employeeposition','users_admin','sup','employees','person'));

	}

	function changeimage(Request $request){

		if ($request->hasFile('avatar')) {
			$avatar = $request->file('avatar');
			$filename = time() . '.' . $avatar->getClientOriginalExtension();
		 	Image::make($avatar)->save(public_path('images/'.$filename));

		 	$user = Auth::user();
		 	$user->avatar = $filename;
		 	$user->save();

		 return redirect('/myprofile');
		}

	}

}
