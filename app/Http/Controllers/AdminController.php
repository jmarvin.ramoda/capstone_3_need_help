<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;
use App\ApplyLeave;
use App\Personal_info;


class AdminController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');    
    $this->middleware('1');
}
    public function admin(){

    $user_id = Auth::user()->id;
    $users = User::find($user_id);
    $test = $users->employee_id;
    $employeeid = EmployeeDetails::find($test);
    $personinfo = Personal_info::find($employeeid)->first();
    $employeedepartment = $employeeid->department;
    $employeeposition = $employeeid->position;

 	$leaverecords = ApplyLeave::where('supervisor_id',$employeeid->id)->get();

	return view('pages.team-leaders-homepage',compact('users','employeedepartment','employeeposition','leaverecords','personinfo'));
        // return view('pages.team-leaders-homepage');
    }


}
