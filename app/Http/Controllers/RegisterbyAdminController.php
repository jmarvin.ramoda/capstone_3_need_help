<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Personal_info;
use App\ShiftSchedule;
use App\EmployeeDetails;
use Illuminate\Support\Facades\Hash;

class RegisterbyAdminController extends Controller{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function supervisorlist(){
        $user_id = Auth::user()->id;
        $users = User::find($user_id);
        $test = $users->employee_id;
        $employeeid = EmployeeDetails::find($test);
        $personinfo = Personal_info::find($employeeid)->first();
        $employeedepartment = $employeeid->department;
        $employeeposition = $employeeid->position;


        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();
    	$schedules =  ShiftSchedule::all();


		return view('auth.registration_by_admin', compact('employeeposition','employeedepartment','users','teamleaders','schedules'));
    }


    public function register(Request $request){
    	$this->validation($request);

        $personinfonew = new Personal_info;
        $personinfonew->first_name = $request->fname;
        $personinfonew->last_name = $request->lname;
        $personinfonew->birthday = $request->dob;
        $personinfonew->email = $request->email;
        $personinfonew->status = $request->mstatus;
        $personinfonew->save();

        $employeedetails = new EmployeeDetails;
        $employeedetails->position = $request->position;
        $employeedetails->rank = $request->rank;
        $employeedetails->department = $request->department;
        $employeedetails->location = $request->location;
        $employeedetails->joining_date = $request->joindate;
        $employeedetails->employment_status = $request->empstatus;
        $employeedetails->reports_to = $request->reports_to;
        $employeedetails->person_id = $personinfonew->id;
        $employeedetails->save();

    	$newuser = new User;
        $newuser->username = $request->username;
        $newuser->password = Hash::make($request->password);
        $newuser->shift_schedule = $request->shift;
        $newuser->role_id = $request->user_type;
        $newuser->employee_id = $employeedetails->id;
        $newuser->email = $request->email;
        $newuser->save();

        return redirect("/tlhomepage")->with('Status','You are registered');


    }

    public function validation($request){
    	
	    return $request->validate([
	        'fname' => 'required|max:255',
	        'lname' => 'required|max:255',
	        'dob' => 'required',
	        'mstatus' => 'required|max:255',
	        'email' => 'required|email|unique:personal_info|max:255',
            'position' => 'required|max:255',
	        'rank' => 'required|max:255',
	        'department' => 'required|max:255',
	        'location' => 'required|max:255',	
	        'reports_to' => 'required|max:255',
	        'joindate' => 'required',
	        'empstatus' => 'required|max:255',
	        'username' => 'required|max:255',
	        'password' => 'required|max:255',
	    ]);

    }

}
