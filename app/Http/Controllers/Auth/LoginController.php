<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

 


    protected function redirectTo( ) {
    if (Auth::check() && Auth::user()->role_id == 1) {
        return '/tlhomepage';
    }
    elseif (Auth::check() && Auth::user()->role_id == 2) {
        return '/home';
    }
 
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   

        // if (Auth::user() && Auth::user()->role_id == 1) {-
            
        //     $redirectTo = '/tlhomepage';

        // } else {
        //     $redirectTo = '/home';
        // }

        $this->middleware('guest')->except('logout');
    }


        protected function guard()
    {
        return Auth::guard();
    }





    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login');
    }

}
