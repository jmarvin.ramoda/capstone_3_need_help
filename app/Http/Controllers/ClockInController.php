<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;
use App\UserAttendanceInfo;
use App\ShiftSchedule;
use App\EmployeeDetails;

class ClockInController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
        
    function show(){

    	echo "test";
    	return redirect('/home');
    }
    
    function login(){
 
    	$user_id = Auth::user()->id;
    	$dt = Carbon::now('UTC');
    	$realtimeDate = $dt->addHours(8);
    	$dateToday = $realtimeDate->year."-".$realtimeDate->month."-".$realtimeDate->day;

        $user = User::where('id', $user_id)->first();
        $shift = ShiftSchedule::where('shift',$user->shift_schedule)->first();

		$loginstance = DB::table('attendance_data')->select('id')->where([ ['user_id', '=', $user_id], ['date', '=', $dateToday], ])->count();

        $dateToday = $realtimeDate->year."-".$realtimeDate->month."-".$realtimeDate->day;
        $currenthour = $realtimeDate;
        $clockintime = $currenthour->hour.":".$currenthour->minute.":".$currenthour->second;

        $schedule = $shift->start_time;

        $actualtimein = Carbon::parse($clockintime);
        $schedule = Carbon::parse($schedule);

        $diff_in_seconds = $schedule->diffInSeconds($actualtimein);
        $hours = floor($diff_in_seconds / 3600);
        $minutes = floor(($diff_in_seconds / 60) % 60);
        $seconds = $diff_in_seconds % 60;


        $duration = $hours.":".$minutes.":".$seconds;
        $phpdate = strtotime($duration);
        $convert = date('H:i:s', $phpdate);

        

        if ($actualtimein < $schedule){

            $status = "On Time";
            $tardy = null;

        }else{
            $status = "Late";
            $tardy = $convert;
        
        }

                               
 		if ($loginstance<>0) {
 			echo $user_id." already logged in ";
 		}else{
 			
	 		$loginnew = new UserAttendanceInfo;

			$dateToday = $realtimeDate->year."-".$realtimeDate->month."-".$realtimeDate->day;
			$currenthour = $realtimeDate;
			$clockintime = $currenthour->hour.":".$currenthour->minute.":".$currenthour->second;

			$loginnew->date = $dateToday;
            $loginnew->log_in = $clockintime;
            $loginnew->status = $status;
			$loginnew->tardiness = $tardy;
			$loginnew->user_id = $user_id;
			
			$loginnew->save();

		}
        
        echo $tardy;



		return redirect('/home');


	}

    function logout(){
 
    	$user_id = Auth::user()->id;
    	$dt = Carbon::now('UTC');
    	$realtimeDate = $dt->addHours(8);

    	$dateToday = $realtimeDate->year."-".$realtimeDate->month."-".$realtimeDate->day;
    	$currenthour = $realtimeDate;

    	$logid = DB::table('attendance_data')->select()->where([ ['user_id', '=', $user_id], ['date', '=', $dateToday], ])->first();
    	$id = $logid->id;

    	UserAttendanceInfo::updateData($id, $currenthour);


        
		return redirect('/home');


	}

}
