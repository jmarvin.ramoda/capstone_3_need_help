<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAttendanceInfo;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\EmployeeDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Personal_info;
use App\ShiftSchedule;

class teamAttendanceController extends Controller
{

    public function __construct()
{
    $this->middleware('auth');    
   
}
	
	function sort(Request $request) {
		$to = date('Y-m-d', strtotime($request->to));
		$from = date('Y-m-d', strtotime($request->from));

		$supervisor = $request->reports_to;

		$user_id_admin = Auth::user();
		$users_admin = User::find($user_id_admin->id);
		$test = $users_admin->employee_id;
		$employees = EmployeeDetails::find($test);
		$employeedepartment = $employees->department;
		$employeeposition = $employees->position;
		$userlogs = $users_admin->userlogs;

		$loginfo = EmployeeDetails::where('rank','Team Member')->get();
							


        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();


        $testdate = $request->filterdatefrom;

	    $workdays = array();
	    $type = CAL_GREGORIAN;
	    $month = date('n'); 
	    $year = date('Y'); 
	    $day_count = cal_days_in_month($type, $month, $year); 


		return view('pages.team_attendance_report_sort',compact( 'testdate','month','year','day_count','teamleaders','loginfo','userlogs','users_admin','employeedepartment','employeeposition', 'to', 'from', 'supervisor'));

	}
    
	function index(Request $request){

		$user_id_admin = Auth::user();
		$users_admin = User::find($user_id_admin->id);
		$test = $users_admin->employee_id;
		$employees = EmployeeDetails::find($test);
		$employeedepartment = $employees->department;
		$employeeposition = $employees->position;
		$userlogs = $users_admin->userlogs;

		$loginfo = EmployeeDetails::where('rank','Team Member')->get();
							


        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();


        $testdate = $request->filterdatefrom;

	    $workdays = array();
	    $type = CAL_GREGORIAN;
	    $month = date('n'); 
	    $year = date('Y'); 
	    $day_count = cal_days_in_month($type, $month, $year); 


		return view('pages.team_attendance_report',compact( 'testdate','month','year','day_count','teamleaders','loginfo','userlogs','users_admin','employeedepartment','employeeposition'));

		
	}



	function ajaxfilter(Request $request){

		$user_id = Auth::user();
		$users = User::find($user_id->id);
		$test = $users->employee_id;
		$employees = EmployeeDetails::find($test);
		$employeedepartment = $employees->department;
		$employeeposition = $employees->position;
		$userlogs = $users->userlogs;

		$supervisor = $request->supervisor;
		$loginfo = EmployeeDetails::where('rank','Team Member')
								  ->where('reports_to',$supervisor)->get();

        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();

 		$testdate = $request->filterdatefrom;
    	$filterdatefrom = $request->filterdatefrom;
    	$filterdateto = $request->filterdateto;



	    $workdays = array();
	    $type = CAL_GREGORIAN;
	    $month = date('n'); // Month ID, 1 through to 12.
	    $year = date('Y'); // Year in 4 digit 2009 format.
	    $day_count = cal_days_in_month($type, $month, $year); // Get the amount of days




		for ($i = 1; $i <= $day_count; $i++){

			
        $date = $year.'/'.$month.'/'.$i; //format date
        $get_name = date('m/d/Y', strtotime($date)); //get week day
        $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars

        $sqldateformat = date('Y-m-d', strtotime($date)); //get week day
		    	foreach ($loginfo as $log){



		    		$person = Personal_info::where('id',$log->person_id)->first();
					
					$emp_id = User::where('employee_id', $log->id)->first();


					$sup_empid = EmployeeDetails::where('id',$log->reports_to)->first();
					$supsearch = Personal_info::where('id', $sup_empid['person_id'])->first();
					$supname = $supsearch['first_name']." ".$supsearch['last_name'];

					$attendanceinfo = UserAttendanceInfo::where('user_id', $emp_id['id'])
														->where('date',$sqldateformat)->first();
														
											
					$logdetails = $attendanceinfo;
					
					$shift = ShiftSchedule::where('shift',$emp_id['shift_schedule'])->first();
					
					$phpdate_starttime = strtotime($attendanceinfo['log_in']);
					$converted_starttime = date('h:i:s A', $phpdate_starttime);					

					$phpdate_endtime = strtotime($attendanceinfo['log_out']);
					$converted_endtime = date('h:i:s A', $phpdate_endtime);

					$phpdate_startsched = strtotime($shift['start_time']);
					$converted_startsched = date('h:i:s A', $phpdate_startsched);

					$phpdate_endsched = strtotime($shift['end_time']);
					$converted_endsched = date('h:i:s A', $phpdate_endsched);
					

					$phpdate = strtotime($attendanceinfo['date']);
					$converted_date = date('m/d/Y', $phpdate);


					$phpdate = strtotime($get_name);
					$get_name_check = date('mdY', $phpdate);					


					$phpdatefrom = strtotime($filterdatefrom);
					$converted_date_from = date('mdY', $phpdatefrom);					


					$phpdateto = strtotime($filterdateto);
					$converted_date_to = date('mdY', $phpdateto);


					$filterfromdate = date('m/d/Y', strtotime($testdate)); //get week day

					$dateexplode = explode('/', $get_name);


					$dateclass = $dateexplode[0].$dateexplode[1].$dateexplode[2];


					if (($converted_date_from > $get_name_check) and ($converted_date_to < $get_name_check)) {


						$test =  "<tr style='display: hidden;'></tr>";
						echo 'Getdate= '.$get_name_check." "."filterdateform =".$converted_date_from." "."filterdateto =".$converted_date_to."<br>";
					
					}else{


							if($get_name == $converted_date){

							$test = 	

								"<tr class='date-".$dateclass."'>
				        				<td>".$get_name."</td>
				        				<td>". $person->first_name." ".$person->last_name."</td>
				        				<td>".$supname."</td>
				        				<td>". $converted_startsched."-".$converted_endsched."</td>
						        		<td>".$converted_starttime."</td>
						        		<td>".$converted_endtime."</td>
						        		<td>".$attendanceinfo->status."</td>
						        		<td>".$attendanceinfo->tardiness."</td>

						        </tr>";


							}else{


								$test = "<tr class='date-".$dateclass."'>
					        				<td>".$get_name ."</td>
					        				<td>".$person->first_name." ".$person->last_name."</td>
					        				<td>".$supname."</td>
					        				<td>".$converted_startsched."-".$converted_endsched."</td>
							        		<td></td>
							        		<td></td>
							        		<td>Absent</td>
							        		<td></td>


						        </tr>";

							}


					

	    			}

	    			
	    			// echo $test;
		       }

		}

				// return view('pages.team_attendance_report',compact('supervisor'));

		
	}


}


