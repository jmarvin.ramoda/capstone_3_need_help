<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAttendanceInfo;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Personal_info;
use App\EmployeeDetails;
use App\ShiftSchedule;

class TeamLeaderHomepageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }	


	function index(){
		$user_id_admin = Auth::user();
		$users_admin = User::find($user_id_admin->id);
		$test = $users_admin->employee_id;
		$employees = EmployeeDetails::find($test);
		$teammembers = EmployeeDetails::where('reports_to',$test)->get();
		$employeedepartment = $employees->department;
		$employeeposition = $employees->position;
		$userlogs = $users_admin->userlogs;

        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();
    	$schedules =  ShiftSchedule::all();

		return view('pages.team-leaders-homepage',compact('teammembers','userlogs','users_admin','employeedepartment','employeeposition','teamleaders','schedules' ));

	}

		function tmemberProfile(Request $request){

		$user_id_admin = Auth::user();
		$users_admin = User::find($user_id_admin->id);

		$user_id = $request->teammember_id;
		$users = User::find($user_id);
		$emp_id = $users->employee_id;
		$employees = EmployeeDetails::find($emp_id);
		$sup_id = EmployeeDetails::find($employees->reports_to);
		$sup = Personal_info::find($sup_id->person_id);
		$person = Personal_info::where("id",$employees->person_id)->first();
		$employeeposition = $employees->position;

        $teamleaders = EmployeeDetails::where('rank', 'Supervisor')
                                    ->orwhere('rank', 'Manager')
                                    ->orwhere('rank', 'Director')->get();
    	$schedules =  ShiftSchedule::all();


    	// echo $users_admin->avatar;
		return view('pages.team_member_profile', compact('users_admin','teammembers','userlogs','users','employees','employees','sup_id','sup','person','employeedepartment','employeeposition','teamleaders','schedules'));
	}


	function tmemberProfileDelete(Request $request){

		$user_id = $request->memberuserid;
		$user = User::find($user_id);
		$employee = EmployeeDetails::find($user->employee_id);

		$user->delete();
		$employee->delete();


		return redirect('/tlhomepage');

	}


	function tmemberProfileEdit(Request $request){
		// $this->validation($request);
		
		$user_id = $request->memberuserid;
		$user = User::find($user_id);

		$employeeedit = EmployeeDetails::find($user->employee_id);
		$employeeedit->position = $request->position;
		$employeeedit->department = $request->department;
		$employeeedit->location = $request->location;
		$employeeedit->reports_to = $request->reports_to;
		$employeeedit->joining_date = $request->joindate;
		$employeeedit->employment_status = $request->empstatus;

		$employeeedit->save();


		$personedit = Personal_info::find($employeeedit->person_id);
		$personedit->first_name = $request->fname;
		$personedit->last_name = $request->lname;
		$personedit->birthday = $request->dob;
		$personedit->status = $request->mstatus;
		$personedit->email = $request->email;
		
		$personedit->save();


		return redirect('/teammemberprofile');

	}


    public function validation($request){
    	
	    return $request->validate([
	        'fname' => 'required|max:191',
	        'lname' => 'required|max:191',
	        'dob' => 'required|date',
	        'mstatus' => 'required|max:191',
	        'email' => 'required|email|unique:personal_info|max:191',
            'position' => 'required|max:191',
	        'rank' => 'required|max:191',
	        'department' => 'required|max:191',
	        'location' => 'required|max:191',	
	        'reports_to' => 'required|max:191',
	        'joindate' => 'required',
	        'empstatus' => 'required|max:191',
	        'memberuserid' => 'required|max:191'
	    ]);

    }

}
