<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Personal_info extends Model
{

	use SoftDeletes;

	protected $table = 'personal_info';
	public $timestamps = false;


    public function Employeedetails(){
        return $this->hasMany('App\EmployeeDetails');
    }


    protected $dates = ['deleted_at'];
}
