<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftSchedule extends Model
{
	protected $table = 'shift_schedules';
	public $timestamps = false;


    public function user(){
    	return $this->belongsTo('App\User');
    }
    
}
