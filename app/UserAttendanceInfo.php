<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAttendanceInfo extends Model
{
	use SoftDeletes;

	protected $table = 'attendance_data';
    public $primaryKey = 'id';
    public $timestamps = false;

    public function user(){
    	return $this->belongsTo('App\User');
    }

    
  public static function updateData($id,$currenthour){
    DB::table('attendance_data')
    ->where('id', $id)
    ->update(['log_out' => $currenthour]);
  }

  protected $dates = ['deleted_at'];

}
